package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

type myHandler struct {
}

var (
	httpDir    http.FileSystem
	apiAddress = "http://127.0.0.1:8100"
	httpClient = &http.Client{
		Transport: &http.Transport{ // 设置超时
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}
)

func toHTTPError(err error) (msg string, httpStatus int) {
	if os.IsNotExist(err) {
		return "404 page not found", http.StatusNotFound
	}
	if os.IsPermission(err) {
		return "403 Forbidden", http.StatusForbidden
	}
	// Default:
	return "500 Internal Server Error", http.StatusInternalServerError
}

func writeError(w http.ResponseWriter, err error) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"status":  "error",
		"message": err.Error(),
	})
}
func (t *myHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	_path := r.URL.Path
	fmt.Println("req path ", _path)

	if strings.Index(_path, "/api/v1") == 0 {
		_u, _ := url.Parse(r.URL.String())
		_u.Host = strings.TrimPrefix(strings.TrimPrefix(apiAddress, "http://"), "https://")
		_u.Scheme = func() string {
			n := strings.Index(apiAddress, "://")
			if n > 0 {
				return apiAddress[0:n]
			}
			return "http"
		}()
		req, err := http.NewRequest(r.Method, _u.String(), nil)
		if err != nil {
			fmt.Printf("%v\n", err)
			writeError(w, err)
			return
		}
		resp, err := httpClient.Do(req)
		if err != nil {
			fmt.Printf("%v\n", err)
			writeError(w, err)
			return
		}
		defer resp.Body.Close()
		for k, v := range resp.Header {
			w.Header().Set(k, v[0])
			for _, v1 := range v[1:] {
				w.Header().Add(k, v1)
			}
		}
		if resp.StatusCode != 200 {
			fmt.Printf("[%v]resp.StatusCode:%v\n", apiAddress, resp.StatusCode)
			w.WriteHeader(resp.StatusCode)
			return
		}
		io.Copy(w, resp.Body)
	} else {
		if _path == "/" {
			_path += "index.html"
		}
		f, err := httpDir.Open(_path)
		if err != nil {
			msg, code := toHTTPError(err)
			http.Error(w, msg, code)
			return
		}
		defer f.Close()

		d, err := f.Stat()
		if err != nil {
			msg, code := toHTTPError(err)
			http.Error(w, msg, code)
			return
		}

		if d.IsDir() {
			http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
			return
		}

		http.ServeContent(w, r, d.Name(), d.ModTime(), f)
	}

}

func main() {
	port := os.Getenv("SERVER_PORT")
	if len(port) == 0 {
		port = "8200"
	}
	addr := os.Getenv("API_SERVER_ADDR")
	if len(addr) > 0 {
		apiAddress = addr
	}
	file, err := os.Executable()
	if err != nil {
		fmt.Println("read dir err", err)
		return
	}
	p, _ := filepath.EvalSymlinks(file)
	httpDir = http.Dir(filepath.Join(filepath.Dir(p), "public"))
	server := http.Server{
		Addr:    fmt.Sprintf(":%v", port),
		Handler: &myHandler{},
	}
	fmt.Println("启动端口:", port)
	server.ListenAndServe()
}
