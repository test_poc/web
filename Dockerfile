###################################
#Build stage
FROM golang:1.19-alpine AS build

#创建目录,并copy代码
WORKDIR /poc_web/
COPY ./ /poc_web

# 编译
RUN go build -o /poc_web/bin/server
RUN cp -r ./public  /poc_web/bin/public

# 拷贝编译出的程序和配置文件到docker镜像中
FROM alpine
ENV TZ Asia/Shanghai
COPY --from=build /poc_web/bin/ /work/

# 定义工作目录为work
WORKDIR /work

# 开放 服务端口
EXPOSE 8200
# 启动http服务
ENTRYPOINT ["./server"]